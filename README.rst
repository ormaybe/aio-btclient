``aiobtclientapi`` is an asynchronous API for communicating with BitTorrent
clients. It is a high-level wrapper around `aiobtclientrpc`_.

.. _aiobtclientrpc: https://codeberg.org/plotski/aiobtclientrpc

All client-specific details are abstracted away as much as possible, but the
individual quirks of each client can still be accessed if necessary.

| Documentation: https://aiobtclientapi.readthedocs.io/
| Repository: https://codeberg.org/plotski/aiobtclientapi
| Package: https://pypi.org/project/aiobtclientapi/

License
-------

`GPLv3+ <https://www.gnu.org/licenses/gpl-3.0.en.html>`_
