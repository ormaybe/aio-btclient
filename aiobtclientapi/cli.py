"""
Basic CLI for manual testing purposes
"""

import argparse
import asyncio
import collections
import inspect
import sys

import aiobtclientapi


def enable_debugging(*modules):
    import logging
    logging.basicConfig(
        format='%(asctime)s.%(msecs)01d %(name)s %(message)s',
        datefmt='%H:%M:%S',
    )

    logger_names = tuple(logging.root.manager.loggerDict)
    for module in modules:
        for name in logger_names:
            if name.startswith(module):
                logging.getLogger(name).setLevel(logging.DEBUG)


def parse_cli_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=(
            'pseudo methods:\n'
            '  monitor_torrents prints an updating table of all torrents.\n'
            '  Positional arguments are partial infohashes or "diff" to only print changes in all torrents.\n'
            '  Keyword arguments:\n'
            '    attrs=<COMMA-SEPARATED TorrentBase attrbiutes>\n'
            '    delay=<SECONDS BETWEEN UPDATES>'
        ),
    )
    parser.add_argument(
        'CLIENT_NAME',
        help=', '.join(aiobtclientapi.client_names()),
    )
    parser.add_argument(
        'CLIENT_URL',
        help='See https://aiobtclientapi.readthedocs.io/en/stable/usage.html#api-urls',
    )
    parser.add_argument(
        'METHOD_NAME',
        help='APIBase method or one of the pseudo methods (see below)',
    )
    parser.add_argument(
        'METHOD_ARGS',
        nargs='*',
        help=(
            'Arguments for METHOD_NAME: '
            'KEY=VALUE (keyword argument), '
            '=VALUE (positional argument), '
            '.METHOD_NAME (APIBase method)'
        ),
    )
    parser.add_argument('--debug', '-d', action='append', default=[])
    args = parser.parse_args()
    if args.METHOD_NAME.startswith('.'):
        # Method name may start with period
        args.METHOD_NAME = args.METHOD_NAME[1:]
    args.METHOD_ARGS = Arguments(args.METHOD_ARGS)
    return args


class Arguments:
    def __init__(self, args):
        self.posargs = []
        self.kwargs = {}
        for arg in args:
            if arg.startswith('='):
                # Positional argument (possibly containing "=")
                self.posargs.append(self._convert_value(arg[1:]))
            elif '=' in arg:
                # Keyword argument
                name, value = arg.split('=', maxsplit=1)
                self.kwargs[name] = self._convert_value(value)
            else:
                # Positional argument
                self.posargs.append(self._convert_value(arg))

    def _convert_value(self, value):
        value = value.rstrip(',')
        if value == 'True':
            return True
        elif value == 'False':
            return False
        else:
            return value

    def resolve_methods(self, api):
        for i, posarg in enumerate(tuple(self.posargs)):
            if isinstance(posarg, str) and posarg.startswith('.'):
                try:
                    self.posargs[i] = getattr(api, posarg[1:])
                except AttributeError:
                    pass

    def __str__(self):
        args = []

        if self.posargs:
            args.append(', '.join(
                (
                    arg.__qualname__
                    if callable(arg)
                    else
                    repr(arg)
                )
                for arg in self.posargs
            ))

        if self.kwargs:
            args.append(', '.join(f'{k}={v!r}' for k, v in self.kwargs.items()))

        return ', '.join(args)

    def __repr__(self):
        return (
            f'{type(self).__name__}('
            + str(self)
            + ')'
        )


async def run_method(api, cli):
    cmd_string = f'{cli.METHOD_NAME}({cli.METHOD_ARGS})'
    print(f'>>> CALLING {cmd_string}')
    method = getattr(api, cli.METHOD_NAME)
    call = method(*cli.METHOD_ARGS.posargs, **cli.METHOD_ARGS.kwargs)

    if inspect.iscoroutine(call):
        try:
            response = await call
        except aiobtclientapi.Warning as e:
            print('???', e)
        except aiobtclientapi.ResponseError as e:
            print('!!!', e, file=sys.stderr)
        else:
            print(f'>>> RESPONSE {cmd_string}')
            if isinstance(response, collections.abc.Mapping):
                for field, value in response.items():
                    print(f'    {field:>18}: {value}')

            elif isinstance(response, str):
                print(f'    {response}')

            elif isinstance(response, collections.abc.Iterable):
                for item in response:
                    print(f'    * {item}')

            else:
                print(f'>>> {response}')

    elif inspect.isasyncgen(call):
        try:
            async for item in call:
                print('   ', item)
        except aiobtclientapi.ResponseError as e:
            print('!!!', e, file=sys.stderr)


async def run_monitor_torrents(api, cli):

    def stringify(cell):
        if isinstance(cell, str):
            return cell
        elif isinstance(cell, collections.abc.Sequence):
            return ','.join((str(x) for x in cell))
        elif isinstance(cell, collections.abc.Mapping):
            return ','.join((f'{k}={v}' for k, v in cell.items()))
        else:
            return str(cell)

    def reduce_torrent(torrent, attrs):
        return {
            attr: torrent[attr]
            for attr in attrs
        }

    async def get_torrents(identifiers, request_attrs, display_attrs, old_torrents={}):
        if identifiers == ['diff']:
            new_torrents = await api.get_torrents(attributes=request_attrs)

            # Only print torrents with different values from previous poll
            tlist = []
            for tnew in new_torrents:
                if tnew.infohash not in old_torrents:
                    # Torrent was added
                    tlist.append(tnew)
                elif (
                        reduce_torrent(old_torrents[tnew.infohash], display_attrs)
                        != reduce_torrent(tnew, display_attrs)
                ):
                    # Torrent has changed
                    tlist.append(tnew)

            old_torrents.clear()
            old_torrents.update(
                (t.infohash, reduce_torrent(t, display_attrs))
                for t in new_torrents
            )
            return tlist

        else:
            try:
                return await api.get_torrents(*identifiers, attributes=request_attrs)
            except aiobtclientapi.UnknownTorrentError as e:
                print(e, file=sys.stderr)
                exit(1)

    while True:
        if 'attrs' in cli.METHOD_ARGS.kwargs:
            request_attrs = cli.METHOD_ARGS.kwargs['attrs'].split(',')
            display_attrs = request_attrs
        else:
            request_attrs = None  # Request all attributes
            display_attrs = api.Torrent.attributes.names

        identifiers = cli.METHOD_ARGS.posargs

        delay = float(cli.METHOD_ARGS.kwargs.get('delay', 1))
        torrents = await get_torrents(identifiers, request_attrs, display_attrs)
        if torrents:
            # Populate cells in table
            table = [display_attrs]
            for torrent in torrents:
                row = []
                for attr in display_attrs:
                    row.append(stringify(getattr(torrent, attr)))
                table.append(row)

            # Find maximum with for each column
            colwidths = [0 for _ in range(len(table[0]))]
            for row in table:
                for i, cell in enumerate(row):
                    colwidths[i] = max(len(cell), colwidths[i])

            # Print pretty table
            for row in table:
                line = []
                for cell, width in zip(row, colwidths):
                    line.append(cell.ljust(width))
                print('|'.join(line))

            print('-' * 80)

        await asyncio.sleep(delay)


async def run_async():
    cli = parse_cli_args()

    enable_debugging(*cli.debug)

    try:
        api = aiobtclientapi.api(
            name=cli.CLIENT_NAME,
            url=cli.CLIENT_URL,
            # timeout=3,
        )
    except aiobtclientapi.ValueError as e:
        print('Failed to create API:', e, file=sys.stderr)
        exit(1)

    api.set_connecting_callback(lambda: print('>>> Connection status changed:', api.status))
    api.set_connected_callback(lambda: print('>>> Connection status changed:', api.status))
    api.set_disconnected_callback(lambda: print('>>> Connection status changed:', api.status))

    cli.METHOD_ARGS.resolve_methods(api)

    try:
        async with api:
            if cli.METHOD_NAME == 'monitor_torrents':
                await run_monitor_torrents(api, cli)
            else:
                await run_method(api, cli)

    except aiobtclientapi.ConnectionError as e:
        print('!!!', e, file=sys.stderr)

def run_sync():
    try:
        asyncio.run(run_async())
    except KeyboardInterrupt:
        pass
