"""
Base class for client APIs
"""

import abc
import functools
import os

import aiobtclientrpc

from ... import errors, torrentdata, utils

import logging  # isort:skip
_log = logging.getLogger(__name__)


class APIBase(abc.ABC):
    """
    Base class for all BitTorrent client APIs

    Subclasses are expected to also inherit from a
    :class:`aiobtclientrpc.RPCBase` subclass. All :meth:`__init__` arguments are
    passed unmodified to :meth:`aiobtclientrpc.RPCBase.__init__`.

    :meth:`call`, :meth:`connect` and :meth:`disconnect` catch most low-level
    exceptions and translate them into :class:`~.errors.ConnectionError`.

    Subclasses must catch :class:`aiobtclientrpc.RPCError` when making RPC calls
    (see :meth:`.aiobtclientrpc.RPCError.translate`) and/or populate
    :attr:`common_rpc_error_map`.

    All asynchronous methods may raise :class:`ConnectionError`.

    :raise ValueError: if the provided connection information is not valid
    """

    def __init__(self, *args, **kwargs):
        # Initialize aiobtclientrpc.*RPC parent class, which may raise
        # ValueError on funky a URL.
        try:
            super().__init__(*args, **kwargs)
        except ValueError as e:
            raise errors.ValueError(e)

        self._torrent_list = torrentdata.TorrentList(self.Torrent)

    async def __aenter__(self):
        return self

    async def __aexit__(self, exception_class, exception, traceback):
        _log.debug('%s: Closing API', self.label)
        await self.disconnect()

    monitor_interval = 0.1
    """Seconds between requests when waiting for an RPC call to take effect"""

    common_rpc_error_map = {}
    """
    Mapping of regular expressions to exceptions for all
    :meth:`~.APIBase.call` calls

    See :meth:`aiobtclientrpc.RPCError.translate`.
    """

    async def call(self, *args, **kwargs):
        """
        Wrapper around :meth:`aiobtclientrpc.RPCBase.call` that handles exceptions

        This is a thin wrapper that translates the following exceptions into
        :class:`~.errors.ConnectionError`:

            * :class:`aiobtclientrpc.ConnectionError`
            * :class:`aiobtclientrpc.TimeoutError`
            * :class:`aiobtclientrpc.AuthenticationError`

        It also converts any common errors by passing
        :attr:`common_rpc_error_map` to
        :meth:`aiobtclientrpc.RPCError.translate`.
        """
        try:
            return await super().call(*args, **kwargs)
        except aiobtclientrpc.AuthenticationError as e:
            raise errors.AuthenticationError(f'{e}: {self.url}')
        except aiobtclientrpc.TimeoutError as e:
            raise errors.TimeoutError(f'{e}: {self.url}')
        except aiobtclientrpc.ConnectionError as e:
            raise errors.ConnectionError(f'{e}: {self.url}')
        except aiobtclientrpc.RPCError as e:
            raise e.translate(self.common_rpc_error_map)

    async def connect(self, *args, **kwargs):
        """
        Wrapper around :meth:`aiobtclientrpc.RPCBase.connect` that handles
        exceptions

        See :meth:`call`.
        """
        try:
            return await super().connect(*args, **kwargs)
        except aiobtclientrpc.AuthenticationError as e:
            raise errors.AuthenticationError(f'{e}: {self.url}')
        except aiobtclientrpc.TimeoutError as e:
            raise errors.TimeoutError(f'{e}: {self.url}')
        except aiobtclientrpc.ConnectionError as e:
            raise errors.ConnectionError(f'{e}: {self.url}')

    async def disconnect(self, *args, **kwargs):
        """
        Wrapper around :meth:`aiobtclientrpc.RPCBase.disconnect` that handles
        exceptions

        See :meth:`call`.
        """
        try:
            return await super().disconnect(*args, **kwargs)
        except aiobtclientrpc.AuthenticationError as e:
            raise errors.AuthenticationError(f'{e}: {self.url}')
        except aiobtclientrpc.TimeoutError as e:
            raise errors.TimeoutError(f'{e}: {self.url}')
        except aiobtclientrpc.ConnectionError as e:
            raise errors.ConnectionError(f'{e}: {self.url}')

    @staticmethod
    def _normalize_infohash(infohash):
        # Concrete clients may need special infohash values (e.g. all upper case)
        return torrentdata.types.Infohash(infohash)

    async def get_infohashes(self):
        """Return sequence of all known infohashes"""
        return tuple(
            self._normalize_infohash(infohash)
            for infohash in await self._get_infohashes()
        )

    @abc.abstractmethod
    async def _get_infohashes(self):
        pass

    @abc.abstractmethod
    async def _get_torrent_fields(self, infohash, *fields):
        pass

    async def _get_torrent_field(self, infohash, field):
        """
        Convenience wrapper around :meth:`_get_torrent_fields` for getting a single
        field
        """
        infohash = self._normalize_infohash(infohash)
        fields = await self._get_torrent_fields(infohash, field)
        return fields[field]

    @functools.cached_property
    def Torrent(self):
        """
        :class:`~.TorrentBase` subclass (should be a return value from
        :func:`~.Torrent_factory`)
        """
        return NotImplemented

    async def get_torrents(self, *, attributes=None):
        r"""
        Return :attr:`~.TorrentList` instance

        :param attributes: Sequence of :attr:`.attrs.TorrentAttribute.name`\ s
            to get for each torrent

            If this is falsy, get all attributes.
        """
        import time
        start = time.monotonic()

        if attributes:
            wanted_attrs = self.Torrent.attributes.only(*attributes)
        else:
            wanted_attrs = self.Torrent.attributes
        raw_torrents = await self._get_torrents(attributes=wanted_attrs)

        _log.debug('Fetched %d raw torrents in %.3fms', len(raw_torrents), (time.monotonic() - start) * 1000)

        await self._torrent_list.update(raw_torrents, wanted_attrs)
        return self._torrent_list

    @abc.abstractmethod
    async def _get_torrents(self):
        """Return mapping of :class:`~.types.Infohash` to raw torrent data"""

    async def add(self, torrent, *, location=None, stopped=False, verify=True):
        """
        Add torrent to client

        :param torrent: Path or URL to torrent file, ``magnet:`` URI or infohash
        :param str location: Download directory or `None` to use the default

            This should be an absolute path. If it isn't, it is made absolute
            based on the current working directory, which may be surprising or
            even non-sensical if the client is running in a different
            environment.
        :param bool stopped: Whether the torrent is active right away
        :param bool verify: Whether any existing files from the torrent are
            hashed by the client to make sure they are not corrupt

        :return: :class:`~.types.Infohash` of the added torrent

        :raise errors.AddTorrentError: if adding fails
        """
        try:
            infohash = await self._add(
                torrent=torrent,
                location=str(location) if location else None,
                stopped=stopped,
                verify=verify,
            )
        except errors.Error as e:
            raise errors.AddTorrentError(e)
        else:
            return torrentdata.types.Infohash(infohash)

    @abc.abstractmethod
    async def _add(self, torrent, **kwargs):
        pass

    async def start(self, infohash):
        """
        Start torrent

        :param infohashes: Infohash of the torrent to start

        :return: `None` when the torrent was started

        :raise errors.StartTorrentError: if starting the torrent failed
        """
        try:
            await self._start(
                self._normalize_infohash(infohash)
            )
        except errors.Error as e:
            raise errors.StartTorrentError(e)

    @abc.abstractmethod
    async def _start(self, infohash):
        pass

    async def stop(self, infohash):
        """
        Stop torrent

        :param infohash: Infohash of the torrent to stop

        :return: `None` when the torrent was stopped

        :raise errors.StopTorrentError: if stopping the torrent failed
        """
        try:
            await self._stop(
                self._normalize_infohash(infohash)
            )
        except errors.Error as e:
            raise errors.StopTorrentError(e)

    @abc.abstractmethod
    async def _stop(self, infohash, **kwargs):
        pass

    async def verify(self, infohash):
        """
        Initiate hash check of a torrent's files

        See also :meth:`verify_wait`.

        :param infohash: Infohash of the torrent to check

        :return: `None` when the verification was initiated

        :raise errors.VerifyTorrentError: if initiating the verification failed
        """
        try:
            await self._verify(
                self._normalize_infohash(infohash)
            )
        except errors.Error as e:
            raise errors.VerifyTorrentError(e)

    async def _verify(self, infohash):
        is_verifying = await self._torrent_is_verifying(infohash)
        if is_verifying:
            raise errors.TorrentAlreadyVerifying(infohash)
        else:
            await self._start_verifying(infohash)

            # Wait for command to take effect
            try:
                await utils.Monitor(
                    call=utils.partial(self._torrent_is_verifying, infohash),
                    interval=0.1,
                    timeout=1.0,
                ).return_value_equals(True)

            except errors.TimeoutError:
                # Verifying may never start because all files are missing or
                # because the torrent is very small and verification was
                # finished before Monitor's first call.
                _log.debug('Verification finished immediately: %r', infohash)

    async def verify_wait(self, *infohashes, interval=(0.3, 3)):
        """
        Asynchronous generator that yields ``(infohash, progress)`` tuples

        ``progress`` is either a number from ``0.0`` to ``100.0`` or
        :class:`~.errors.Error` or :class:`~.errors.Warning`.

        Every infohash is yielded at least once.

        :param infohashes: Infohashes of the torrents
        :param interval: Delay between progress updates

            ``seconds``
                Always use the same delay.

            ``(seconds_min, seconds_max)``

                Dynamically change the interval based on how much of the torrent
                is verified. ``seconds_min`` is used until ``progress`` gets
                close to ``100.0``, then ``seconds_min`` gradually moves to
                ``seconds_max``.

                This means update requests are spread out until the end, where
                you want to check progress more frequently to report the end of
                the verification accurately.
        """
        if isinstance(interval, (int, float)):
            interval_min = interval_max = interval
        elif isinstance(interval, tuple) and len(interval) == 2:
            interval_min, interval_max = interval
        else:
            raise TypeError(f'Invalid interval: {interval!r}')

        waiters = [
            self._verify_wait(
                self._normalize_infohash(infohash),
                interval_min,
                interval_max,
            )
            for infohash in infohashes
        ]

        async for coro in utils.merge_async_generators(*waiters):
            infohash, progress_or_exception = await coro
            yield (infohash, progress_or_exception)

    async def _verify_wait(self, infohash, interval_min, interval_max):
        assert isinstance(infohash, torrentdata.types.Infohash)

        interval = utils.DynamicInterval(
            name=infohash,
            min=interval_min,
            max=interval_max,
            progress_getter=utils.partial(self._get_verifying_progress, infohash),
        )

        try:
            # Initial progress
            yield (infohash, await self._get_verifying_progress(infohash))

            progress = 0
            while await self._torrent_is_verifying(infohash):
                if interval.progress != progress:
                    yield (infohash, interval.progress)
                    progress = interval.progress

                await interval.sleep()

            # Final progress
            yield (infohash, await self._get_verifying_progress(infohash))

        except (errors.Error, errors.Warning) as e:
            yield (infohash, e)

    @abc.abstractmethod
    async def _torrent_is_verifying(self, infohash):
        """`True` if torrent is verifying or queued for verification"""

    @abc.abstractmethod
    async def _start_verifying(self, infohash):
        """Initiate hash check"""

    @abc.abstractmethod
    async def _get_verifying_progress(self, infohash):
        """
        Verifying progress in percent (0 to 100)

        After verification is done (:meth:`_torrent_is_verifying` returns
        `False`), return the combined download progress of the wanted files in
        percent.
        """
