from ... import torrentdata

import logging  # isort:skip
_log = logging.getLogger(__name__)


class Infohash(torrentdata.attrs.Infohash):
    dependencies = (
        'hash',
    )

    def _get_value(raw):
        return raw['hash']


class IsDiscovering(torrentdata.attrs.IsDiscovering):
    dependencies = (
        'state',
    )

    def _get_value(raw):
        return raw['state'] == 'metaDL'


class IsDownloading(torrentdata.attrs.IsDownloading):
    dependencies = (
        'dlspeed',
    )

    def _get_value(raw):
        return raw['dlspeed'] > 0


class IsQueued(torrentdata.attrs.IsQueued):
    dependencies = (
        'state',
    )

    def _get_value(raw):
        return raw['state'].startswith('queued')


class IsStopped(torrentdata.attrs.IsStopped):
    dependencies = (
        'state',
    )

    def _get_value(raw):
        return raw['state'].startswith('paused')


class IsUploading(torrentdata.attrs.IsUploading):
    dependencies = (
        'upspeed',
    )

    def _get_value(raw):
        return raw['upspeed'] > 0


class IsVerifying(torrentdata.attrs.IsVerifying):
    dependencies = (
        'state',
    )

    def _get_value(raw):
        return raw['state'].startswith('checking')


class IsIdle(torrentdata.attrs.IsIdle):
    dependencies = torrentdata.attrs.join_dependencies(
        IsStopped,
        IsDownloading,
        IsUploading,
        # IsConnected,  # NotImplemented
        IsDiscovering,
    )

    def _get_value(raw):
        return (
            not IsStopped._get_value(raw)
            and not IsDownloading._get_value(raw)
            and not IsUploading._get_value(raw)
            # and not IsConnected._get_value(raw)  # NotImplemented
            and not IsDiscovering._get_value(raw)
        )


class IsSeeding(torrentdata.attrs.IsSeeding):
    dependencies = torrentdata.attrs.join_dependencies(
        'progress',
        IsStopped,
        IsVerifying,
        IsQueued,
    )

    def _get_value(raw):
        return (
            raw['progress'] >= 1.0
            and not IsStopped.get_value(raw)
            and not IsVerifying.get_value(raw)
            and not IsQueued.get_value(raw)
        )


class Name(torrentdata.attrs.Name):
    dependencies = (
        'name',
    )

    def _get_value(raw):
        return raw['name']


class PeersSeeding(torrentdata.attrs.PeersSeeding):
    dependencies = (
        'num_complete',
    )

    def _get_value(raw):
        return raw['num_complete']
