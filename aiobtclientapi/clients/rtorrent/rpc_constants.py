TORRENT_FIELDS = {
    # Some basic torrent info
    'd': (
        'name',
        'hash',
    ),

    # File fields
    'f': (
        # Relative file path within torrent without torrent name ("/"-separated string)
        'path',
        # File size in bytes
        'size_bytes',
    ),

    # Peer fields
    'p': (
    ),

    # Tracker fields
    't': (
        # Last time there was an attempt to announce, regardless of success
        'activity_time_last',
        # Whether rtorrent is using this tracker
        'is_enabled',
        # Number of failed requests (resets to 0 on success)
        'failed_counter',
        # 1: HTTP, 2: UDP, 3: DHT
        'type',
        # Number of seeders
        'scrape_complete',
        # Number of leechers
        'scrape_incomplete',
        # Last time there was an attempt to scrape this tracker
        'scrape_time_last',
        # Announce URL
        'url',
    ),
}
