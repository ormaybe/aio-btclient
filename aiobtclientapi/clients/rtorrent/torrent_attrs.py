from ... import torrentdata

import logging  # isort:skip
_log = logging.getLogger(__name__)


class Infohash(torrentdata.attrs.Infohash):
    dependencies = (
        'd.hash',
    )

    def _get_value(raw):
        return raw['d.hash']


class IsConnected(torrentdata.attrs.IsConnected):
    dependencies = (
        'd.peers_connected',
    )

    def _get_value(raw):
        return raw['d.peers_connected'] > 0


class IsDiscovering(torrentdata.attrs.IsDiscovering):
    dependencies = (
        'd.is_meta',
    )

    def _get_value(raw):
        return raw['d.is_meta'] == 1


class IsDownloading(torrentdata.attrs.IsDownloading):
    dependencies = (
        'd.down.rate',
    )

    def _get_value(raw):
        return raw['d.down.rate'] > 0


class IsIsolated(torrentdata.attrs.IsIsolated):
    dependencies = (
        'd.is_private',
        't',
    )

    def _get_value(raw):
        if not raw['d.is_private']:
            # DHT is used
            return False

        # Torrent has trackers?
        trackers = raw['t']
        if trackers:
            # Any tracker responded properly?
            if any(tracker['t.failed_counter'] == 0 for tracker in trackers):
                return False

        # No way to find any peers
        return True


class IsStopped(torrentdata.attrs.IsStopped):
    dependencies = (
        'd.is_open',
        'd.is_active',
    )

    def _get_value(raw):
        return raw['d.is_open'] == 0 and raw['d.is_active'] == 0


class IsUploading(torrentdata.attrs.IsUploading):
    dependencies = (
        'd.up.rate',
    )

    def _get_value(raw):
        return raw['d.up.rate'] > 0


class IsVerifying(torrentdata.attrs.IsVerifying):
    dependencies = (
        'd.is_hash_checking',
    )

    def _get_value(raw):
        return raw['d.is_hash_checking'] == 1


class IsIdle(torrentdata.attrs.IsIdle):
    dependencies = torrentdata.attrs.join_dependencies(
        IsStopped,
        IsDownloading,
        IsUploading,
        IsConnected,
        IsDiscovering,
    )

    def _get_value(raw):
        return (
            not IsStopped.get_value(raw)
            and not IsDownloading.get_value(raw)
            and not IsUploading.get_value(raw)
            and not IsConnected.get_value(raw)
            and not IsDiscovering.get_value(raw)
        )


class IsSeeding(torrentdata.attrs.IsSeeding):
    dependencies = torrentdata.attrs.join_dependencies(
        'd.left_bytes',
        IsStopped,
        IsVerifying,
    )

    def _get_value(raw):
        return (
            raw['d.left_bytes'] <= 0
            and not IsStopped.get_value(raw)
            and not IsVerifying.get_value(raw)
        )


class Name(torrentdata.attrs.Name):
    dependencies = (
        'd.name',
    )

    def _get_value(raw):
        return raw['d.name']


class PeersSeeding(torrentdata.attrs.PeersSeeding):
    dependencies = (
        't',
    )

    def _get_value(raw):
        scraped_trackers = tuple(
            t for t in raw['t']
            if t['t.scrape_time_last'] > 0
        )
        if scraped_trackers:
            return max(
                t['t.scrape_complete']
                for t in scraped_trackers
            )
        else:
            return torrentdata.types.Count.UNKNOWN
