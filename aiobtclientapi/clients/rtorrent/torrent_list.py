import asyncio
import collections
import functools

from . import rpc_constants

import logging  # isort:skip
_log = logging.getLogger(__name__)


class RawRtorrents:

    _forced_dependencies = {
        'd.hash',
    }

    def __init__(self, api):
        self._api = api

    async def get(self, attributes):
        torrents = await self._get_torrents(attributes)
        infohashes = tuple(t['d.hash'] for t in torrents)
        files, peers, trackers = await asyncio.gather(
            self._get_list(infohashes, 'f', attributes),
            self._get_list(infohashes, 'p', attributes),
            self._get_list(infohashes, 't', attributes),
        )
        if files:
            for torrent, flist in zip(torrents, files):
                torrent['f'] = flist
        if peers:
            for torrent, flist in zip(torrents, files):
                torrent['f'] = flist
        if trackers:
            for torrent, tlist in zip(torrents, trackers):
                torrent['t'] = tlist

        return {
            t['d.hash']: t
            for t in torrents
        }

    async def _get_torrents(self, attributes):
        fields = self._get_fields('d', attributes)
        table = await self._api.call('d.multicall2', '', '', *(f'{f}=' for f in fields))
        return tuple(
            {
                field: value
                for field, value in zip(fields, values)
            }
            for values in table
        )

    async def _get_list(self, infohashes, multicall_prefix, attributes):
        fields = self._get_fields(multicall_prefix, attributes)
        if not fields:
            return ()
        else:
            cmd_fields = tuple(f'{f}=' for f in fields)
            cmds = tuple(
                (f'{multicall_prefix}.multicall', infohash, '', *cmd_fields)
                for infohash in infohashes
            )
            table = await self._api._multicall_rt(*cmds)

            # Return sequence of torrents. Each torrent is a sequence of items
            # (files, peers, trackers, etc). Each item is a dictionary.
            return tuple(
                tuple(
                    {
                        field: value
                        for field, value in zip(fields, values)
                    }
                    for values in torrent
                )
                for torrent in table
            )

    _all_sections = ('d', 'f', 'p', 't')

    def _get_fields(self, section, attributes):
        # Extract only those fields from `self._fields_sections[section]` that
        # are in `attributes.dependencies`
        dependencies = self._forced_dependencies.union(attributes.dependencies)
        if section in dependencies:
            # Whole section wanted (e.g. "t")
            wanted_fields = self._fields_sections[section]
        else:
            # Only some fields from section wanted (e.g. "d.name")
            section_fields = self._fields_sections[section]
            wanted_fields = tuple(
                field
                for field in section_fields
                if field in dependencies
            )
        return wanted_fields

    @functools.cached_property
    def _fields_sections(self):
        # Separate dependencies of all existing attributes into {
        #     "d": ('d.hash', 'd.name', ...),
        #     "f": ("f.path", "f.size_bytes", ...),
        #     "t": ("t.url", ...),
        #     ...
        # }
        #
        # Everything except "d" is taken from hardcoded TORRENT_FIELDS.
        # TorrentAttributes don't require specific fields like "t.url", they
        # just need "t".
        sections = collections.defaultdict(set)
        for field in self._api.Torrent.attributes.dependencies:
            sec, name = (
                field.split('.', maxsplit=1)
                if '.' in field else
                (None, None)
            )
            if sec == 'd':
                # `field` is "<section>.<name>" (e.g. "d.name")
                sections[sec].add(f'{sec}.{name}')
            else:
                # `field` is a whole section ("d", "f", "p" or "t")
                sections[field].update(
                    f'{field}.{name}'
                    for name in rpc_constants.TORRENT_FIELDS[field]
                )

        return {
            sec: tuple(sections[sec])
            for sec in self._all_sections
        }
