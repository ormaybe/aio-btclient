"""
RPC constants

Reference: https://raw.githubusercontent.com/transmission/transmission/main/libtransmission/transmission.h
"""

import enum

UNKNOWN_SEEDS_COUNT = -1


class TR_STATUS(enum.IntEnum):
    STOPPED = 0        # Torrent is stopped
    CHECK_WAIT = 1     # Queued to check files
    CHECK = 2          # Checking files
    DOWNLOAD_WAIT = 3  # Queued to download
    DOWNLOAD = 4       # Downloading
    SEED_WAIT = 5      # Queued to seed
    SEED = 6           # Seeding
