from ... import torrentdata
from . import rpc_constants

import logging  # isort:skip
_log = logging.getLogger(__name__)


class Infohash(torrentdata.attrs.Infohash):
    dependencies = (
        'hashString',
    )

    def _get_value(raw):
        return raw['hashString']


class IsConnected(torrentdata.attrs.IsConnected):
    dependencies = (
        'peersConnected',
    )

    def _get_value(raw):
        return raw['peersConnected'] > 0


class IsDiscovering(torrentdata.attrs.IsDiscovering):
    dependencies = (
        'metadataPercentComplete',
        'status',
    )

    def _get_value(raw):
        return (
            raw['metadataPercentComplete'] < 1
            and raw['status'] != rpc_constants.TR_STATUS.STOPPED
        )


class IsDownloading(torrentdata.attrs.IsDownloading):
    dependencies = (
        'rateDownload',
    )

    def _get_value(raw):
        return raw['rateDownload'] > 0


class IsIsolated(torrentdata.attrs.IsIsolated):
    dependencies = (
        'isPrivate',
        'trackerStats',
    )

    def _get_value(raw):
        if not raw['isPrivate']:
            # DHT is used
            # TODO: Somehow find out if DHT is enabled/disabled globally.
            return False

        # Torrent has trackers?
        trackerStats = raw['trackerStats']
        if trackerStats:
            # Did we try to connect to a tracker?
            if any(tracker['hasAnnounced'] for tracker in trackerStats):
                # Did at least one tracker respond?
                if any(tracker['lastAnnounceSucceeded'] for tracker in trackerStats):
                    return False
            # We didn't try yet - assume non-isolation
            else:
                return False

        # No way to find any peers
        return True


class IsQueued(torrentdata.attrs.IsQueued):
    dependencies = (
        'status',
    )

    def _get_value(raw):
        return raw['status'] in (
            rpc_constants.TR_STATUS.CHECK_WAIT,
            rpc_constants.TR_STATUS.DOWNLOAD_WAIT,
            rpc_constants.TR_STATUS.SEED_WAIT,
        )


class IsStopped(torrentdata.attrs.IsStopped):
    dependencies = (
        'status',
    )

    def _get_value(raw):
        return raw['status'] == rpc_constants.TR_STATUS.STOPPED


class IsUploading(torrentdata.attrs.IsUploading):
    dependencies = (
        'rateUpload',
    )

    def _get_value(raw):
        return raw['rateUpload'] > 0


class IsVerifying(torrentdata.attrs.IsVerifying):
    dependencies = (
        'status',
    )

    def _get_value(raw):
        return raw['status'] in (
            rpc_constants.TR_STATUS.CHECK,
            rpc_constants.TR_STATUS.CHECK_WAIT,
        )


class IsIdle(torrentdata.attrs.IsIdle):
    dependencies = torrentdata.attrs.join_dependencies(
        IsStopped,
        IsDownloading,
        IsUploading,
        IsConnected,
        IsDiscovering,
    )

    def _get_value(raw):
        return (
            not IsStopped.get_value(raw)
            and not IsDownloading.get_value(raw)
            and not IsUploading.get_value(raw)
            and not IsConnected.get_value(raw)
            and not IsDiscovering.get_value(raw)
        )


class IsSeeding(torrentdata.attrs.IsSeeding):
    dependencies = torrentdata.attrs.join_dependencies(
        'percentDone',
        IsStopped,
        IsVerifying,
    )

    def _get_value(raw):
        return (
            raw['percentDone'] >= 1.0
            and not IsStopped.get_value(raw)
            and not IsVerifying.get_value(raw)
        )


class Name(torrentdata.attrs.Name):
    dependencies = (
        'name',
    )

    def _get_value(raw):
        return raw['name']


class PeersSeeding(torrentdata.attrs.PeersSeeding):
    dependencies = (
        'trackerStats',
    )

    def _get_value(raw):
        scraped_trackers = tuple(
            t for t in raw['trackerStats']
            if t['lastScrapeSucceeded']
        )
        if scraped_trackers:
            seeders = max(
                t['seederCount']
                for t in scraped_trackers
            )
            if seeders == rpc_constants.UNKNOWN_SEEDS_COUNT:
                return torrentdata.types.Count.UNKNOWN
            else:
                return seeders
        else:
            return torrentdata.types.Count.UNKNOWN
