import functools
import sys

from .. import constants, utils
from . import types

import logging  # isort:skip
_log = logging.getLogger(__name__)


class TorrentAttributes(tuple):
    """
    Sequence of :class:`~.TorrentAttribute` subclasses

    :param attributes: :class:`~.TorrentAttribute` subclasses
    :param bool add_missing: Whether to add any missing default attributes

    :raise ValueError: if any `attributes` are not a subclass of
        :class:`TorrentAttribute`
    """

    @classmethod
    @functools.cache
    def _get_all_parents(cls):
        return {
            parent.name: parent
            for parent in utils.find_subclasses(TorrentAttribute, sys.modules[__name__])
        }

    @classmethod
    @functools.cache
    def _get_default_attribute(cls, parent_cls):
        name = parent_cls.__name__ + '_NotImplemented'
        bases = (parent_cls,)
        namespace = {}
        cls = type(name, bases, namespace)
        return cls

    @classmethod
    def from_module(cls, module):
        """
        Create :class:`TorrentAttributes` from :class:`TorrentAttribute`
        subclasses found in `module`

        Any missing attributes are filled in by default implementations that
        always return `NotImplemented`.

        :param module: :class:`types.ModuleType` object (i.e. something you can
            import)
        """
        attributes = utils.find_subclasses(TorrentAttribute, module)

        # Fill in missing default attributes
        names = set(attr.name for attr in attributes)
        not_implemented = []
        for parent_name, parent_cls in cls._get_all_parents().items():
            if parent_name not in names:
                not_implemented.append(cls._get_default_attribute(parent_cls))
        attributes += tuple(not_implemented)
        return cls(*attributes)

    def __new__(cls, *attributes):
        # Validate arguments
        for attr in attributes:
            if not utils.is_subclass(attr, TorrentAttribute):
                raise ValueError(f'Not a TorrentAttribute subclass: {attr!r}')

        # Deduplicate while preserving order
        return super().__new__(cls, list(dict.fromkeys(attributes)))

    @functools.cached_property
    def names(self):
        r"""Sequence of :attr:`~.TorrentAttribute.name`\ s"""
        return tuple(attr.name for attr in self)

    @functools.cached_property
    def as_dict(self):
        r"""
        Mapping of :attr:`~.TorrentAttribute.name`\ s to
        :class:`~.TorrentAttribute` instances
        """
        return {attr.name: attr for attr in self}

    @functools.lru_cache(maxsize=3)
    def only(self, *names):
        r"""
        Return new :class:`~.TorrentAttributes` with less attributes

        :param names: Sequence of :attr:`~.TorrentAttribute.name`\ s
        """
        as_dict = self.as_dict
        try:
            attributes = (
                as_dict[name]
                for name in names
            )
            return TorrentAttributes(*attributes)
        except KeyError as e:
            raise ValueError(f'No such attribute: {e}')

    @functools.cached_property
    def dependencies(self):
        """
        Deduplicated sequence of all :attr:`~.TorrentAttribute.dependencies`
        of all attributes combined
        """
        return tuple(sorted(set(
            dep
            for attr in self
            for dep in attr.dependencies
        )))


class TorrentAttribute:
    """Get one piece of normalized torrent information from raw RPC response"""

    name = NotImplemented
    """Name of the attribute"""

    dependencies = ()
    """Sequence of RPC fields that are required to compute the value"""

    @classmethod
    def get_value(cls, raw):
        """
        Compute and normalize the attribute's value from raw RPC response

        `NotImplemented` from :meth:`_get_value` is translated into
        :attr:`~.constants.NOT_IMPLEMENTED`.

        :param raw: Torrent object from RPC response
        """
        try:
            value = cls._get_value(raw)
        except KeyError as e:
            # Trying to access attribute that wasn't requested, i.e. `raw` is
            # missing a dependency
            raise AttributeError(f'Missing dependency for attribute {cls.name!r}: {e}')
        else:
            if value is NotImplemented:
                return constants.NOT_IMPLEMENTED
            else:
                return cls._normalize_value(value)

    @classmethod
    def _get_value(cls, raw):
        """
        Get the raw value (e.g. infohash :class:`str`) or `NotImplemented`

        :param raw: Torrent object from RPC response
        """
        return NotImplemented

    @classmethod
    def _normalize_value(cls, value):
        """
        Normalize the value from :meth:`_get_value` (e.g. convert infohash
        from :class:`str` to :class:`~.Infohash`)

        :param raw: Torrent object from RPC response
        """
        raise NotImplementedError(f'{cls.__qualname__}._normalize_value() is not implemented')


def join_dependencies(*dependencies):
    """
    Join :attr:`~.TorrentAttribute.dependencies` from multiple attributes

    :param dependencies: Actual dependencies (e.g. :class:`str`) and
        :class:`TorrentAttribute` subclasses
    """
    flat_deps = []
    for dep in dependencies:
        if isinstance(dep, str):
            flat_deps.append(dep)
        elif utils.is_subclass(dep, TorrentAttribute):
            flat_deps.extend(dep.dependencies)
        else:
            raise ValueError(f'Invalid dependency: {dep!r}')
    return tuple(sorted(set(flat_deps)))


class Infohash(TorrentAttribute):
    """Unique torrent hash"""

    name = 'infohash'

    @staticmethod
    def _normalize_value(value):
        return types.Infohash(value)


class IsConnected(TorrentAttribute):
    """Torrent is connected to peers"""

    name = 'is_connected'

    @staticmethod
    def _normalize_value(value):
        return bool(value)


class IsDiscovering(TorrentAttribute):
    """Torrent is (trying to) download the metadata from the DHT"""

    name = 'is_discovering'

    @staticmethod
    def _normalize_value(value):
        return bool(value)


class IsDownloading(TorrentAttribute):
    """Torrent is using downstream bandwidth"""

    name = 'is_downloading'

    @staticmethod
    def _normalize_value(value):
        return bool(value)


class IsIdle(TorrentAttribute):
    """
    Torrent is not paused while also not downloading, uploading, verifying,
    etc
    """

    name = 'is_idle'

    @staticmethod
    def _normalize_value(value):
        return bool(value)


class IsIsolated(TorrentAttribute):
    """Torrent is unable to find new peers"""

    name = 'is_isolated'

    @staticmethod
    def _normalize_value(value):
        return bool(value)


class IsQueued(TorrentAttribute):
    """Torrent is queued for downloading, uploading, verifying, etc"""

    name = 'is_queued'

    @staticmethod
    def _normalize_value(value):
        return bool(value)


class IsSeeding(TorrentAttribute):
    """Torrent has downloaded all wanted files and is not stopped"""

    name = 'is_seeding'

    @staticmethod
    def _normalize_value(value):
        return bool(value)


class IsStopped(TorrentAttribute):
    """Torrent is not allowed to cause any traffic"""

    name = 'is_stopped'

    @staticmethod
    def _normalize_value(value):
        return bool(value)


class IsUploading(TorrentAttribute):
    """Torrent is using upstream bandwidth"""

    name = 'is_uploading'

    @staticmethod
    def _normalize_value(value):
        return bool(value)


class IsVerifying(TorrentAttribute):
    """Torrent content is being hashed to check integrity of stored files"""

    name = 'is_verifying'

    @staticmethod
    def _normalize_value(value):
        return bool(value)


class Name(TorrentAttribute):
    """Torrent's top-level file/directory name"""

    name = 'name'

    @staticmethod
    def _normalize_value(value):
        return utils.normalize_unicode(value)


class PeersSeeding(TorrentAttribute):
    """Number of seeding peers"""

    name = 'peers_seeding'

    @staticmethod
    def _normalize_value(value):
        return types.Count(value)
