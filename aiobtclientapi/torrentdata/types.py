"""Types of :class:`~.TorrentBase` attributes"""

import re

from .. import errors, utils


class TypeBase:
    # With tens of instances per torrent and hundreds or thousands of torrents,
    # this should save MiBs of memory.
    #
    # NOTE: Each subclass must also declare `__slots__ = ()` or this has no
    #       effect.
    __slots__ = ()

    def __repr__(self):
        return f'{type(self).__name__}({super().__repr__()})'


class _TypeMeta(type):
    __constant_name_regex = re.compile(r'^[A-Z][A-Z0-9_]+[A-Z]$')

    def __new__(cls, name, bases, namespace):
        bases = (TypeBase,) + bases
        return super().__new__(cls, name, bases, namespace)

    def __init__(cls, name, bases, namespace):
        # Turn type constants into instances of the class they are attached to
        for name, value in tuple(namespace.items()):
            if cls.__constant_name_regex.search(name):
                setattr(cls, name, cls(value))


class Count(int, metaclass=_TypeMeta):
    """Number from 0 to < infinity"""

    UNKNOWN = -1

    def __new__(cls, value):
        if value < 0 and value != cls.UNKNOWN:
            raise errors.ValueError(f'Invalid {cls.__name__}: {value!r}')
        else:
            return super().__new__(cls, value)

    def __str__(self):
        if self == self.UNKNOWN:
            return '?'
        else:
            return str(int(self))


class Infohash(str, metaclass=_TypeMeta):
    """Case-insensitive infohash string (40 hexadecimal digits)"""

    __slots__ = ()

    NULL = '0000000000000000000000000000000000000000'

    def __new__(cls, value):
        string = str(value)
        if not utils.is_infohash_regex.search(string):
            raise errors.ValueError(f'Invalid infohash: {value!r}')
        else:
            return super().__new__(cls, string.lower())

    def __eq__(self, other):
        if isinstance(other, str):
            return self.lower() == other.lower()
        else:
            return NotImplemented

    # To stay hashable, we must define __hash__() if we define __eq__().
    # https://docs.python.org/3/reference/datamodel.html#object.__hash__
    def __hash__(self):
        return super().__hash__()


class Ratio(float, metaclass=_TypeMeta):
    """Torrent's upload/download ratio as :class:`float`"""

    __slots__ = ()

    INFINITE = float('inf')
    NOT_APPLICABLE = -1.0

    def __str__(self):
        if self == self.INFINITE:
            return '∞'
        elif self == self.NOT_APPLICABLE:
            return ''
        else:
            return str(float(self))

    def __repr__(self):
        return f'{type(self).__name__}({repr(float(self))})'
