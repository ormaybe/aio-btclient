import re
import string

_allowed_constant_characters = ''.join((
    string.ascii_letters,
    string.digits,
    '_'
))
_constants_cache = {}


class Constant:
    """Base class of all constants returned by :func:`get`"""
    pass


def get(name, value):
    """
    Create unique constant

    Constants are instances of unique classes. The constant's class is a
    subclass of `type(value)` and :class:`Constant`.

    A constant of the value `value` should behace exactly like `value` itself,
    except that the constant has a ``name`` property and a special
    :meth:`__repr__`.

    Calling this function with the same arguments returns the same constant
    object from a cache.
    """
    clsname = 'Constant_' + re.sub(
        rf'[^{_allowed_constant_characters}]',
        '_',
        name,
    ).upper()
    const = _constants_cache.get(clsname, None)

    if const is None:
        def __repr__(self):
            return f'<Constant {self._name}={self._value!r}>'

        bases = (type(value), Constant)
        namespace = {
            '__module__': __name__,
            '__qualname__': f'{__name__}.get({name!r}, {value!r})',
            '__repr__': __repr__,
            '_name': name,
            '_value': value,
            'name': property(lambda self: self._name),
        }
        cls = type(clsname, bases, namespace)
        const = _constants_cache[clsname] = cls(value)

    if const != value:
        raise RuntimeError(f'Cannot create same constants with different values: {const!r} != {value!r}')
    else:
        return const
