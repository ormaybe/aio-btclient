#!/bin/bash

set -o nounset
set -o errexit


if [ -z "${1:-}" ]; then
    echo 'Usage:'
    echo "$0 CLIENT_NAME METHOD_NAME [METHOD_ARGS ...]"
    echo ''
    echo 'Generate RPC_URL from file rpc-connections.env and call'
    echo ''
    echo '  btclient RPC_URL METHOD_NAME [METHOD_ARGS ...]'
    echo ''
    echo 'See btclient -h for more information.'
    exit 1
fi


RPC_CONNECTIONS="$(dirname "$0")/rpc-connections.env"

get_rpc_variable_name() {
    local client="$1"
    local name="$2"
    echo "${client}_RPC_${name}" | tr '[a-z]' '[A-Z]'
}

get_rpc_variable_value() {
    local client="$1"
    local name="$2"
    local varname="$(get_rpc_variable_name "$client" "$name")"
    local vardef="$(cat "$RPC_CONNECTIONS" | grep "$varname=")"
    if [ -n "$vardef" ]; then
        eval "export $vardef ; echo \"\$$varname\""
    fi
}

get_rpc_url() {
    local client="$1"
    local scheme="$(get_rpc_variable_value "$client_name" 'scheme')"
    local username="$(get_rpc_variable_value "$client_name" 'username')"
    local password="$(get_rpc_variable_value "$client_name" 'password')"
    local host="$(get_rpc_variable_value "$client_name" 'host')"
    local port="$(get_rpc_variable_value "$client_name" 'port')"

    local url=""
    [ -n "$scheme" ] && url="$url$scheme://"
    [ -n "$username" ] && [ -n "$password" ] && url="$url$username:$password@"
    url="$url$host:$port"
    echo "$url"
}


client_name="$1" ; shift
client_url="$(get_rpc_url "$client_name")"
btclient "$client_name" "$client_url" "$@"
