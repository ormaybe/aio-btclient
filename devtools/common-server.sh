CLI_ADD_TORRENTS='false'

while [ -n "${1:-}" ]; do
    case "$1" in
        -a|--add-torrents)    if [ -z "${2:-}" ]; then
                                  # Add fixed number of varied torrents
                                  shift; CLI_ADD_TORRENTS='true'
                              elif echo "$2" | grep -Pq '^\d+$'; then
                                  # Add specific number of random torrents
                                  shift; CLI_ADD_TORRENTS="$1"
                                  shift
                              elif echo "$2" | grep -Pq '^(true|false)$'; then
                                  # Add specific number of varied torrents or add no torrents
                                  shift; CLI_ADD_TORRENTS="$1"
                                  shift
                              else
                                  echo "--add-torrents paremeter must be true, false or integer: $1" >&2
                                  exit 1
                              fi
                              ;;
        -?*)                  echo "Unknown option: $1" >&2
                              exit 1;;
    esac
done
