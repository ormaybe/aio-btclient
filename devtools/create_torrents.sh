#!/bin/bash

set -o nounset
set -o errexit

source "$(dirname "$0")/utils.sh"


main() {
    if [ -d "$CLI_BASEDIR" ]; then
        if [ "$CLI_PURGE" = 'true' ]; then
            echo "Purging $CLI_BASEDIR" >&2
            rm -rf "$CLI_BASEDIR"
        else
            echo "Adding more torrents to $CLI_BASEDIR" >&2
        fi
    fi

    local torrentdir="$CLI_BASEDIR/torrents"
    local contentdir="$CLI_BASEDIR/content"
    local downloaddir="$CLI_BASEDIR/downloads"

    create_and_add_torrents_via_rpc \
        "$torrentdir" "$contentdir" "$downloaddir" \
        "$CLI_COUNT" \
        "$CLI_CLIENT_NAME" "$CLI_CLIENT_URL"

    tree "$CLI_BASEDIR"
}

usage() {
    echo "Create torrents from randomly created content and optinally add it to a client"
    echo ''
    echo 'Adding the torrents to a client requires the btclient executable from '
    echo 'aiobtclientapi to be in the path.'
    echo ''
    echo 'USAGE'
    echo "  $0 [-c COUNT] [-n CLIENT_NAME] [-n CLIENT_URL]"
    echo ''
    echo 'OPTIONS'
    echo '  -b,--basedir BASEDIR   Base directory for created files'
    echo "                         Default: $CLI_BASEDIR"
    echo '  -c,--count COUNT       How many random torrents to create'
    echo '                         Default: Create fixed number of varied torrents'
    echo '  -p,--purge             Whether to purge BASEDIR first'
    echo '  -n,--name CLIENT_NAME  Add the created torrents to this client'
    echo '  -u,--url CLIENT_URL    RPC URL of CLIENT_NAME'
    echo '  -d,--done              Whether added torrents are completely downloaded'
    echo '  -D,--not-done          Whether added torrents are not downloaded'
    echo '  -h,--help              Show this help screen and exit'
    exit 0
}

CLI_BASEDIR='/tmp/random_torrents'
CLI_COUNT='true'
CLI_PURGE='false'
CLI_CLIENT_NAME=''
CLI_CLIENT_URL=''
CLI_DONE='random'

parse_args() {
    while [ -n "${1:-}" ]; do
        case "$1" in
            -b|--basedir)  if [ -n "${2:-}" ]; then
                               shift; CLI_BASEDIR="$1"
                           else
                               echo "Missing argument for $1" >&2
                               exit 1
                           fi
                           shift;;
            -c|--count)    if [ -n "${2:-}" ]; then
                               shift; CLI_COUNT="$1"
                           else
                               echo "Missing argument for $1" >&2
                               exit 1
                           fi
                           shift;;
            -p|--purge)    CLI_PURGE='true'
                           shift;;
            -n|--name)     if [ -n "${2:-}" ]; then
                               shift; CLI_CLIENT_NAME="$1"
                           else
                               echo "Missing argument for $1" >&2
                               exit 1
                           fi
                           shift;;
            -u|--url)      if [ -n "${2:-}" ]; then
                               shift; CLI_CLIENT_URL="$1"
                           else
                               echo "Missing argument for $1" >&2
                               exit 1
                           fi
                           shift;;
            -d|--done)     CLI_DONE='true'
                           shift;;
            -D|--not-done) CLI_DONE='false'
                           shift;;
            -h|--help)     usage
                           shift;;
            -?*)           echo "Unknown option: $1" >&2
                           exit 1
                           shift;;
            *)             echo "Unknown option: $1" >&2
                           exit 1
                           shift ;;
            --)            shift; break;;  # End of options
        esac
    done
}

parse_args "$@"
main
