#!/bin/bash

if [ "$1" = "--ui" ]; then
    run_ui=true
else
    run_ui=false
fi

set -o nounset   # Don't allow unset variables

homedir="$(realpath "$(dirname "$0")")"
projectdir="$(realpath "$homedir/../..")"
workdir=/tmp/deluge.aiobtclientrpc
contentdir="$workdir/contents/"
torrentdir="$workdir/torrents/"
downloaddir="$workdir/downloads"
watchdir="$workdir/add_torrents"
configdir="$workdir/config"

source "$(dirname "$homedir")/rpc-connections.env"
source "$(dirname "$homedir")/utils.sh"
source "$(dirname "$homedir")/common-server.sh"

mkdir -p "$workdir/config"
mkdir -p "$downloaddir"
mkdir -p "$watchdir"

deluged_pid='no such pid'
delugegtk_pid='no such pid'
cleanup() {
    [ -e "/proc/$deluged_pid" ] && kill "$deluged_pid"
    [ -e "/proc/$delugegtk_pid" ] && kill "$delugegtk_pid"
    wait "$deluged_pid" "$delugegtk_pid"
    rm -r "$workdir"
}

trap cleanup EXIT


echo "$DELUGE_RPC_USERNAME:$DELUGE_RPC_PASSWORD:10" > "$configdir/auth"

cat <<-EOF > "$configdir/core.conf"
{
    "file": 1,
    "format": 1
}{
    "add_paused": false,
    "allow_remote": false,
    "daemon_port": $DELUGE_RPC_PORT,
    "download_location": "$workdir/downloads",
    "enabled_plugins": [
        "AutoAdd"
    ],
    "move_completed_path": "$workdir/downloads",
    "listen_ports": [
        54321,
        54322
    ],
    "new_release_check": false,
    "outgoing_interface": "",
    "outgoing_ports": [
        0,
        0
    ],
    "random_outgoing_ports": false,
    "random_port": false,
    "torrentfiles_location": "$workdir/torrents",
    "dht": false,
    "lsd": false,
    "natpmp": false,
    "upnp": false,
    "utpex": false
}
EOF

cat <<-EOF > "$configdir/gtk3ui.conf"
{
    "file": 1,
    "format": 1
}{
    "autoadd_queued": true,
    "autoconnect": true,
    "autoconnect_host_id": "d52e54e6ec084822bda81d626e2e70e7",
    "autostart_localhost": false,
    "check_new_releases": false,
    "close_to_tray": false,
    "enable_system_tray": false,
    "show_connection_manager_on_start": false,
    "show_new_releases": false,
    "standalone": false,
    "start_in_tray": false
}
EOF

cat <<-EOF > "$configdir/hostlist.conf"
{
    "file": 3,
    "format": 1
}{
    "hosts": [
        [
            "d52e54e6ec084822bda81d626e2e70e7",
            "$DELUGE_RPC_HOST",
            $DELUGE_RPC_PORT,
            "$DELUGE_RPC_USERNAME",
            "$DELUGE_RPC_PASSWORD"
        ]
    ]
}
EOF


cat <<-EOF > "$configdir/autoadd.conf"
{
    "file": 2,
    "format": 1
}{
    "file": 2,
    "format": 1,
    "next_id": 2,
    "watchdirs": {
        "1": {
            "abspath": "$watchdir",
            "add_paused": true,
            "add_paused_toggle": false,
            "append_extension": ".added",
            "append_extension_toggle": true,
            "auto_managed": true,
            "auto_managed_toggle": false,
            "copy_torrent": "/",
            "copy_torrent_toggle": false,
            "delete_copy_torrent_toggle": false,
            "download_location": "$workdir/downloads",
            "download_location_toggle": false,
            "enabled": true,
            "label": "initial",
            "label_toggle": false,
            "max_connections": 0,
            "max_connections_toggle": false,
            "max_download_speed": 0.0,
            "max_download_speed_toggle": false,
            "max_upload_slots": 0,
            "max_upload_slots_toggle": false,
            "max_upload_speed": 0.0,
            "max_upload_speed_toggle": false,
            "move_completed": true,
            "move_completed_path": "/",
            "move_completed_toggle": false,
            "owner": "$DELUGE_RPC_USERNAME",
            "path": "$watchdir",
            "queue_to_top": true,
            "queue_to_top_toggle": false,
            "remove_at_ratio": true,
            "remove_at_ratio_toggle": false,
            "seed_mode": false,
            "stop_at_ratio": true,
            "stop_at_ratio_toggle": false,
            "stop_ratio": 0.0,
            "stop_ratio_toggle": false
        }
    }
}}
EOF


(
    sleep 1
    create_and_add_torrents_via_watchdir "$torrentdir" "$contentdir" "$downloaddir" "$CLI_ADD_TORRENTS" "$watchdir"
) &

deluged --loglevel info --do-not-daemonize --config "$configdir" &
deluged_pid="$!"

deluge-gtk --config "$configdir" &
delugegtk_pid="$!"

wait "$delugegtk_pid"
