#!/bin/bash

set -o nounset   # Don't allow unset variables
set -o errexit   # Exit if any command fails

if [ -n "${1:-}" ]; then
    if echo "$1" |  grep -Pq '^(socket|scgi|http)$'; then
        mode="$1"
        shift
    else
        echo "Unknown mode argument $1 (expected socket, scgi, http)" >&2
        exit 1
    fi
else
    echo 'Missing mode argument: socket, scgi, http' >&2
    exit 1
fi

homedir="$(realpath "$(dirname "$0")")"
projectdir="$(realpath "$homedir/../..")"
workdir=/tmp/rtorrent.aiobtclientrpc
contentdir="$workdir/contents/"
torrentdir="$workdir/torrents/"
downloaddir="$workdir/downloads"
watchdir="$workdir/add_torrents"
socketfile="$workdir/rpc.socket"
configfile="$workdir/rtorrent.rc"

nginxdir="$(realpath "$(dirname "$0")")/nginx"
nginxpidfile="$workdir/nginx.pid"
nginxconfigfile="$workdir/nginx.conf"
htpasswdfile="$workdir/htpasswd"

source "$(dirname "$homedir")/rpc-connections.env"
source "$(dirname "$homedir")/utils.sh"
source "$(dirname "$homedir")/common-server.sh"

mkdir -p "$workdir"
mkdir -p "$nginxdir"
mkdir -p "$downloaddir"


cleanup() {
    kill_http_server
    rm -r "$workdir"
}

trap cleanup EXIT


config_socket() {
    cat <<-END >> "$configfile"
network.scgi.open_local = $socketfile
schedule2 = scgi_permission,0,0,"execute.nothrow=chmod,\"go-rwx,o=\",$socketfile"
END
}

config_scgi_server() {
    cat <<-END >> "$configfile"
network.scgi.open_port = $RTORRENT_RPC_HOST:$RTORRENT_RPC_PORT
END
}

config_watch_directory() {
    mkdir -p "$watchdir"
    cat <<-END >> "$configfile"
schedule2 = watch_start, 1, 1, ((load.start_verbose, (cat, "$watchdir/*.torrent")))
END
}


kill_http_server() {
    if [[ -e "$nginxpidfile" ]]; then
        kill "$(cat "$nginxpidfile")"
    fi
}

run_http_server() {
    if [ ! -z "$RTORRENT_RPC_USERNAME" ]; then
        echo "$RTORRENT_RPC_PASSWORD" | htpasswd -i -c "$htpasswdfile" "$RTORRENT_RPC_USERNAME"
    else
        rm -f "$htpasswdfile"
    fi

    cat <<-EOF > "$nginxconfigfile"
	error_log ${workdir}/nginx.error.log;
	pid ${nginxpidfile};

	events {
	    worker_connections 768;
	}

	http {
	    server {
	        listen 127.0.0.1:${RTORRENT_RPC_PORT};
	        error_log ${workdir}/nginx.error.log;
	        access_log ${workdir}/nginx.access.log;

	        auth_basic "Restricted";
	        auth_basic_user_file ${htpasswdfile};

	        location /RPC2 {
	           include $nginxdir/scgi_params;
	           scgi_pass unix:${socketfile};
               client_body_temp_path ${workdir};
	        }
	    }
	}
	EOF

    echo "### NGINX CONFIG"
    cat $nginxconfigfile

    /usr/sbin/nginx -c "$nginxconfigfile"
}


config_watch_directory
if [[ "$mode" = "socket" ]]; then
    echo "###### SOCKET MODE"
    config_socket
elif [[ "$mode" = "scgi" ]]; then
    echo "###### SCGI MODE"
    config_scgi_server
elif [[ "$mode" = "http" ]]; then
    echo "###### HTTP MODE"
    config_socket
    run_http_server
else
    echo "Unknown mode: $mode" >&2
    exit 1
fi

echo "### RTORRENT CONFIG"
cat $configfile

create_and_add_torrents_via_watchdir "$torrentdir" "$contentdir" "$downloaddir" "$CLI_ADD_TORRENTS" "$watchdir"

rtorrent -d "$downloaddir" -s "$workdir" -o "import=$configfile" -o "import=$homedir/rtorrent.rc"
