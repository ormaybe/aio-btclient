Usage
=====

This guide explains how to use ``aiobtclientapi``.

Basics
------

First, you need to create an instance of one of the :class:`~.clients.APIBase`
subclasses.

.. code-block:: python

    api_dg = aiobtclientapi.DelugeAPI("AzureDiamond:hunter2@localhost:1234")
    api_qb = aiobtclientapi.QbittorrentAPI("AzureDiamond:hunter2@localhost:1234")
    api_rt = aiobtclientapi.RtorrentAPI("path/to/rpc.socket")
    api_tm = aiobtclientapi.TransmissionAPI("AzureDiamond:hunter2@localhost:1234")

You can also instantiate one of the subclassses by passing a client name to
:func:`~.aiobtclientapi.api`. :func:`~.aiobtclientapi.client_names` returns a
sorted sequence of valid client names.

.. code-block:: python

    client_name = "rtorrent"
    url = "path/to/rpc.socket"
    api = aiobtclientapi.api(client_name, url)

API URLs
--------

Most clients accept URLs like ``[USERNAME:PASSWORD@]HOST[:PORT]``.

Below are the exact URL formats for each client. Uppercase words are
placeholders and square brackets (``[]``) mark optional parts.

    Deluge
        | ``[USERNAME:PASSWORD@]HOST[:PORT]``

    qBittorrent
        | ``[http[s]://][USERNAME:PASSWORD@]HOST[:PORT]``

    rTorrent
        | ``[scgi://]HOST[:PORT]``
        | ``[file://]SOCKET_PATH``
        | ``http[s]://[USERNAME:PASSWORD@]HOST[:PORT][/PATH]``

    Transmission
        | ``[http[s]://][USERNAME:PASSWORD@]HOST[:PORT][/PATH]``

RPC Calls
---------

If there is no API method for an operation, you also can :meth:`~.APIBase.call`
arbtitrary RPC methods. Be aware that the RPC protocols might change or work
unexpectedly in edge cases. Error cases are also generally ignored or reported
with generic messages.

See the :mod:`aiobtclientrpc` documentation for details.

.. code-block:: python

   api = aiobtclientapi.TransmissionAPI("AzureDiamond:hunter2@localhost:1234")
   info = await api.call("session-get")
   info['arguments']['version']
   >>> 3.21 (d34db33f)

Virtual Environment
-------------------

There is a ``Makefile`` with a ``venv`` target that should set up everything you
need. You can undo this by simply deleting the ``venv`` directory.

.. code-block:: bash

   $ make venv
   $ source venv/bin/activate

Command Line Tool
-----------------

``aiobtclientapi`` comes with a very crude CLI command, ``btclient``, that
allows you to call API methods and inspect the response. It is intended for
manual testing, not for productive use, but it is useful for debugging and
figuring out RPC protocols. You might also find it useful to get started.

Example Usage
^^^^^^^^^^^^^

.. code-block:: bash

   $ btclient qbittorrent 'AzureDiamond:hunter2@localhost' get_infohashes
   >>> CALLING get_infohashes()
   >>> Connection status changed: ConnectionStatus.connecting
   >>> Connection status changed: ConnectionStatus.connected
   >>> RESPONSE get_infohashes()
       * 16c2ddc7d354a1d839821f59c95a1489cbd82199
       * 3393cc21966a63b96da8a3e75b4650ff3bb4a576
       * 412a1db3ec177a31a4efb28ca4febc06f016583a
       * 4435ef55af79b350e7b85d5b330a7886a61e3bdf
       * d5a34e9eb4709e265f0f03a1c8ab60890dcb94a9
       * f98fd369bbcf3416b0271dbaae757c25a8867de7
   >>> Connection status changed: ConnectionStatus.disconnected
   $ btclient qbittorrent 'AzureDiamond:hunter2@localhost' stop 4435ef55af79b350e7b85d5b330a7886a61e3bdf
   >>> CALLING stop('4435ef55af79b350e7b85d5b330a7886a61e3bdf')
   >>> Connection status changed: ConnectionStatus.connecting
   >>> Connection status changed: ConnectionStatus.connected
   >>> RESPONSE stop('4435ef55af79b350e7b85d5b330a7886a61e3bdf')
   >>> None
   >>> Connection status changed: ConnectionStatus.disconnected
   $ btclient qbittorrent 'AzureDiamond:hunter2@localhost' call app/version
   >>> CALLING call('app/version')
   >>> Connection status changed: ConnectionStatus.connecting
   >>> Connection status changed: ConnectionStatus.connected
   >>> RESPONSE call('app/version')
       v4.5.2
   >>> Connection status changed: ConnectionStatus.disconnected
