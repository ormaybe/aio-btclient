import os
import re
import types
from unittest.mock import AsyncMock, Mock, call

import aiobtclientrpc
import pytest

from aiobtclientapi import errors, torrentdata
from aiobtclientapi.clients import base


class MyURL(aiobtclientrpc.URL):
    default = 'mybt://localhost:123'


class MyRPC(aiobtclientrpc.RPCBase):
    label = 'MyBT'
    name = 'mybt'
    URL = MyURL
    _call = AsyncMock()
    _connect = AsyncMock()
    _disconnect = AsyncMock()


def MockAPI():
    class MockAPI(base.APIBase, MyRPC):
        _get_infohashes = AsyncMock()
        _get_torrent_fields = AsyncMock()
        Torrent = Mock()
        _get_torrents = AsyncMock()
        _add = AsyncMock()
        _start = AsyncMock()
        _stop = AsyncMock()
        _torrent_is_verifying = AsyncMock()
        _start_verifying = AsyncMock()
        _get_verifying_progress = AsyncMock()

    return MockAPI()

@pytest.fixture
def api():
    return MockAPI()


@pytest.mark.asyncio
async def test_init_raises_ValueError(mocker):
    mocker.patch('aiobtclientrpc.RPCBase.__init__', side_effect=ValueError('foo'))
    with pytest.raises(errors.ValueError, match=r'^foo$'):
        MockAPI()


@pytest.mark.asyncio
async def test_context_manager(api, mocker):
    mocker.patch('aiobtclientapi.clients.APIBase.disconnect')

    async with api as api_:
        assert api.disconnect.call_args_list == []
        assert api is api_
    assert api.disconnect.call_args_list == [call()]


@pytest.mark.parametrize(
    argnames='return_value, exception, exp_result',
    argvalues=(
        ('foo', None, 'foo'),
        (None, aiobtclientrpc.ConnectionError('no net'), errors.ConnectionError('no net: {url}')),
        (None, aiobtclientrpc.TimeoutError('timeout'), errors.TimeoutError('timeout: {url}')),
        (None, aiobtclientrpc.AuthenticationError('bad password'), errors.AuthenticationError('bad password: {url}')),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_call(return_value, exception, exp_result, api, mocker):
    call_mock = mocker.patch('aiobtclientrpc.RPCBase.call', side_effect=exception, return_value=return_value)

    if isinstance(exp_result, Exception):
        exp_msg = str(exp_result).format(url=api.url)
        with pytest.raises(type(exp_result), match=rf'^{re.escape(exp_msg)}$'):
            await api.call(foo='bar', baz=123)
    else:
        retval = await api.call(foo='bar', baz=123)
        assert retval == exp_result

    assert call_mock.call_args_list == [call(foo='bar', baz=123)]


@pytest.mark.asyncio
async def test_call_translates_RPCError(api, mocker):
    api.common_rpc_error_map[r'^foo$'] = RuntimeError('bar')

    mocker.patch('aiobtclientrpc.RPCBase.call', side_effect=aiobtclientrpc.RPCError('foo'))
    with pytest.raises(RuntimeError, match=r'^bar$'):
        await api.call()

    mocker.patch('aiobtclientrpc.RPCBase.call', side_effect=aiobtclientrpc.RPCError('asdf'))
    with pytest.raises(aiobtclientrpc.RPCError, match=r'^asdf$'):
        await api.call()


@pytest.mark.parametrize(
    argnames='return_value, exception, exp_result',
    argvalues=(
        ('foo', None, 'foo'),
        (None, aiobtclientrpc.ConnectionError('no net'), errors.ConnectionError('no net: {url}')),
        (None, aiobtclientrpc.TimeoutError('timeout'), errors.TimeoutError('timeout: {url}')),
        (None, aiobtclientrpc.AuthenticationError('bad password'), errors.AuthenticationError('bad password: {url}')),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_connect(return_value, exception, exp_result, api, mocker):
    connect_mock = mocker.patch('aiobtclientrpc.RPCBase.connect', side_effect=exception, return_value=return_value)

    if isinstance(exp_result, Exception):
        exp_msg = str(exp_result).format(url=api.url)
        with pytest.raises(type(exp_result), match=rf'^{re.escape(exp_msg)}$'):
            await api.connect(foo='bar', baz=123)
    else:
        retval = await api.connect(foo='bar', baz=123)
        assert retval == exp_result

    assert connect_mock.call_args_list == [call(foo='bar', baz=123)]


@pytest.mark.parametrize(
    argnames='return_value, exception, exp_result',
    argvalues=(
        ('foo', None, 'foo'),
        (None, aiobtclientrpc.ConnectionError('no net'), errors.ConnectionError('no net: {url}')),
        (None, aiobtclientrpc.TimeoutError('timeout'), errors.TimeoutError('timeout: {url}')),
        (None, aiobtclientrpc.AuthenticationError('bad password'), errors.AuthenticationError('bad password: {url}')),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_disconnect(return_value, exception, exp_result, api, mocker):
    disconnect_mock = mocker.patch('aiobtclientrpc.RPCBase.disconnect', side_effect=exception, return_value=return_value)

    if isinstance(exp_result, Exception):
        exp_msg = str(exp_result).format(url=api.url)
        with pytest.raises(type(exp_result), match=rf'^{re.escape(exp_msg)}$'):
            await api.disconnect(foo='bar', baz=123)
    else:
        retval = await api.disconnect(foo='bar', baz=123)
        assert retval == exp_result

    assert disconnect_mock.call_args_list == [call(foo='bar', baz=123)]


def test_normalize_infohash(api):
    return_value = api._normalize_infohash('f000000000000000000000000000000000000000')
    assert isinstance(return_value, torrentdata.types.Infohash)
    assert return_value == 'f000000000000000000000000000000000000000'


@pytest.mark.asyncio
async def test_get_infohashes(api, mocker):
    infohashes = (
        'a00000000000000000000000000000000000000a',
        'B00000000000000000000000000000000000000B',
        'C00000000000000000000000000000000000000c',
    )

    mocker.patch.object(api, '_get_infohashes', AsyncMock(return_value=infohashes))
    return_value = await api.get_infohashes()
    assert return_value == infohashes
    for item in return_value:
        assert isinstance(item, torrentdata.types.Infohash)


@pytest.mark.asyncio
async def test_get_torrent_field(api, mocker):
    api._get_torrent_fields.return_value = {'bar': 123, 'baz': 456}
    assert await api._get_torrent_field('f000000000000000000000000000000000000000', 'bar') == 123
    assert api._get_torrent_fields.call_args_list == [
        call('f000000000000000000000000000000000000000', 'bar'),
    ]
    assert await api._get_torrent_field('f000000000000000000000000000000000000000', 'baz') == 456
    assert api._get_torrent_fields.call_args_list == [
        call('f000000000000000000000000000000000000000', 'bar'),
        call('f000000000000000000000000000000000000000', 'baz'),
    ]


@pytest.mark.parametrize(
    argnames='torrent, location, exp_location, stopped, verify, call_result, exp_result',
    argvalues=(
        (
            'mock.torrent',
            '', None,
            'maybe stopped',
            'maybe verify',
            'd34db33f',
            "Infohash('d34db33f')",
        ),
        (
            'mock.torrent',
            'path/to/location', 'path/to/location',
            'maybe stopped',
            'maybe verify',
            errors.Error('something went wrong'),
            errors.AddTorrentError(errors.Error('something went wrong')),
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_add(torrent, location, exp_location, stopped, verify, call_result, exp_result, api, mocker):
    if isinstance(call_result, Exception):
        api._add.side_effect = call_result
    else:
        api._add.return_value = call_result
    mocker.patch('aiobtclientapi.torrentdata.types.Infohash', return_value=f'Infohash({call_result!r})')

    coro = api.add(
        torrent,
        location=location,
        stopped=stopped,
        verify=verify,
    )
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            await coro
    else:
        return_value = await coro
        assert return_value == exp_result

    assert api._add.call_args_list == [call(
        torrent=torrent,
        location=exp_location,
        stopped=stopped,
        verify=verify,
    )]


@pytest.mark.parametrize(
    argnames='infohash, call_result, exp_result',
    argvalues=(
        (
            'f00',
            None,
            None,
        ),
        (
            'f00',
            errors.Error('something went wrong'),
            errors.StartTorrentError(errors.Error('something went wrong')),
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_start(infohash, call_result, exp_result, api, mocker):
    if isinstance(call_result, Exception):
        api._start.side_effect = call_result
    else:
        api._start.return_value = call_result
    mocker.patch.object(api, '_normalize_infohash', return_value=lambda h: f'normalized:{h}')

    coro = api.start(infohash)
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            await coro
    else:
        return_value = await coro
        assert return_value == exp_result

    assert api._start.call_args_list == [call(api._normalize_infohash.return_value)]
    assert api._normalize_infohash.call_args_list == [call(infohash)]


@pytest.mark.parametrize(
    argnames='infohash, call_result, exp_result',
    argvalues=(
        (
            'f00',
            None,
            None,
        ),
        (
            'f00',
            errors.Error('something went wrong'),
            errors.StopTorrentError(errors.Error('something went wrong')),
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_stop(infohash, call_result, exp_result, api, mocker):
    if isinstance(call_result, Exception):
        api._stop.side_effect = call_result
    else:
        api._stop.return_value = call_result
    mocker.patch.object(api, '_normalize_infohash', return_value=lambda h: f'normalized:{h}')

    coro = api.stop(infohash)
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            await coro
    else:
        return_value = await coro
        assert return_value == exp_result

    assert api._stop.call_args_list == [call(api._normalize_infohash.return_value)]
    assert api._normalize_infohash.call_args_list == [call(infohash)]


@pytest.mark.parametrize(
    argnames='infohash, call_result, exp_result',
    argvalues=(
        (
            'f00',
            None,
            None,
        ),
        (
            'f00',
            errors.Error('something went wrong'),
            errors.VerifyTorrentError(errors.Error('something went wrong')),
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_verify(infohash, call_result, exp_result, api, mocker):
    if isinstance(call_result, Exception):
        mocker.patch.object(api, '_verify', side_effect=call_result)
    else:
        mocker.patch.object(api, '_verify', return_value=call_result)
    mocker.patch.object(api, '_normalize_infohash', return_value=lambda h: f'normalized:{h}')

    coro = api.verify(infohash)
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            await coro
    else:
        return_value = await coro
        assert return_value == exp_result

    assert api._verify.call_args_list == [call(api._normalize_infohash.return_value)]
    assert api._normalize_infohash.call_args_list == [call(infohash)]


@pytest.mark.parametrize(
    argnames='infohash, torrent_is_verifying, start_verifying_exception, monitor_result, exp_results',
    argvalues=(
        pytest.param(
            'f00',
            True,
            None,
            [],
            errors.TorrentAlreadyVerifying('f00'),
            id='Torrent is already being verified',
        ),
        pytest.param(
            'f00',
            False,
            None,
            [False, False, True],
            None,
            id='Torrent verification is started',
        ),
        pytest.param(
            'f00',
            False,
            errors.UnknownTorrentError(id='f00'),
            None,
            errors.UnknownTorrentError(id='f00'),
            id='Exception from verification command is raised',
        ),
        pytest.param(
            'f00',
            False,
            None,
            errors.TimeoutError('no time left!'),
            None,
            id='Timeout is ignored',
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test__verify(infohash, torrent_is_verifying, start_verifying_exception, monitor_result, exp_results, api, mocker):
    partial_mock = mocker.patch('aiobtclientapi.utils.partial')
    Monitor_mock = mocker.patch('aiobtclientapi.utils.Monitor', return_value=Mock(
        return_value_equals=AsyncMock(side_effect=monitor_result),
    ))

    api._torrent_is_verifying.return_value = torrent_is_verifying
    api._start_verifying.side_effect = start_verifying_exception

    if isinstance(exp_results, BaseException):
        with pytest.raises(type(exp_results), match=rf'^{re.escape(str(exp_results))}$'):
            await api._verify(infohash)
    else:
        results = await api._verify(infohash)
        assert results == exp_results

    assert api._torrent_is_verifying.call_args_list == [call(infohash)]

    if not torrent_is_verifying:
        assert api._start_verifying.call_args_list == [call(infohash)]
    else:
        assert api._start_verifying.call_args_list == []

    if not torrent_is_verifying and not start_verifying_exception:
        assert Monitor_mock.call_args_list == [call(
            call=partial_mock.return_value,
            interval=0.1,
            timeout=1,
        )]
        assert partial_mock.call_args_list == [call(api._torrent_is_verifying, infohash)]
        assert Monitor_mock.return_value.return_value_equals.call_args_list == [call(True)]
    else:
        assert Monitor_mock.call_args_list == []
        assert partial_mock.call_args_list == []
        assert Monitor_mock.return_value.return_value_equals.call_args_list == []


@pytest.mark.parametrize(
    argnames='interval, exp_exception',
    argvalues=(
        (None, None),
        (123, None),
        ((1, 2), None),
        ((1, 2, 3), TypeError('Invalid interval: (1, 2, 3)')),
        ('foo', TypeError("Invalid interval: 'foo'")),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_verify_wait(interval, exp_exception, api, mocker):
    infohashes = ('f00', '8a5', '8a2')

    mocker.patch('aiobtclientapi.clients.base.APIBase._normalize_infohash', side_effect=lambda h: f'normalized:{h}')
    mocker.patch('aiobtclientapi.clients.base.APIBase._verify_wait')
    mocker.patch('aiobtclientapi.utils.merge_async_generators', return_value=AsyncMock(
        __class__=types.AsyncGeneratorType,
        __aiter__=lambda self: self,
        __anext__=AsyncMock(side_effect=(
            [
                AsyncMock(return_value=(infohash, i))()
                for infohash, i in zip(infohashes, range(100))
            ]
            if not exp_exception else None
        )),
    ))

    exp_progresses = [
        (infohashes[0], 0),
        (infohashes[1], 1),
        (infohashes[2], 2),
    ]
    if interval is None:
        exp_interval_min, exp_interval_max = 0.3, 3
    elif isinstance(interval, (int, float)):
        exp_interval_min = exp_interval_max = interval
    elif isinstance(interval, tuple) and len(interval) == 2:
        exp_interval_min, exp_interval_max = interval

    kwargs = {}
    if interval is not None:
        kwargs['interval'] = interval
    agen = api.verify_wait(*infohashes, **kwargs)

    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            [(infohash, progress) async for infohash, progress in agen]
    else:
        progresses = [(infohash, progress) async for infohash, progress in agen]
        assert progresses == exp_progresses

        assert api._verify_wait.call_args_list == [
            call('normalized:' + infohashes[i], exp_interval_min, exp_interval_max)
            for i in range(len(exp_progresses))
        ]


@pytest.mark.parametrize(
    argnames=(
        'infohash, torrent_is_verifying_results, get_verifying_progress_results, '
        'exp_yields, exp_exception'
    ),
    argvalues=(
        # Non-infohash argument
        (
            'f00?',
            [],
            [],
            [],
            AssertionError(''),
        ),
        # Yield until torrent stops verifying
        (
            torrentdata.types.Infohash('f00000000000000000000000000000000000000f'),
            [True, True, False, 'ignored', 'ignored'],
            [0.123, 17.456],
            [
                (torrentdata.types.Infohash('f00000000000000000000000000000000000000f'), 0.123),
                (torrentdata.types.Infohash('f00000000000000000000000000000000000000f'), 'mock interval.progress'),
                (torrentdata.types.Infohash('f00000000000000000000000000000000000000f'), 17.456),
            ],
            None,
        ),
        # Yield error
        (
            torrentdata.types.Infohash('f00000000000000000000000000000000000000f'),
            [True, True, errors.Error('no'), 'ignored', 'ignored'],
            [88, 99],
            [
                (torrentdata.types.Infohash('f00000000000000000000000000000000000000f'), 88),
                (torrentdata.types.Infohash('f00000000000000000000000000000000000000f'), 'mock interval.progress'),
                (torrentdata.types.Infohash('f00000000000000000000000000000000000000f'), errors.Error('no')),
            ],
            None,
        ),
        # Yield warning
        (
            torrentdata.types.Infohash('f00000000000000000000000000000000000000f'),
            [True, True, errors.Warning('huh'), 'ignored', 'ignored'],
            [88, 99],
            [
                (torrentdata.types.Infohash('f00000000000000000000000000000000000000f'), 88),
                (torrentdata.types.Infohash('f00000000000000000000000000000000000000f'), 'mock interval.progress'),
                (torrentdata.types.Infohash('f00000000000000000000000000000000000000f'), errors.Warning('huh')),
            ],
            None,
        ),
        # Raise other exception
        (
            torrentdata.types.Infohash('f00000000000000000000000000000000000000f'),
            [True, True, RuntimeError('argh'), 'ignored', 'ignored'],
            [88, 99],
            [],
            RuntimeError('argh'),
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test__verify_wait(infohash, torrent_is_verifying_results, get_verifying_progress_results,
                            exp_yields, exp_exception, api, mocker):
    partial_mock = mocker.patch('aiobtclientapi.utils.partial')
    DynamicInterval_mock = mocker.patch(
        'aiobtclientapi.utils.DynamicInterval',
        return_value=Mock(
            sleep=AsyncMock(),
            progress='mock interval.progress',
        ),
    )

    api._torrent_is_verifying.side_effect = torrent_is_verifying_results
    api._get_verifying_progress.side_effect = get_verifying_progress_results

    agen = api._verify_wait(infohash, 'interval min', 'interval max')
    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            [(ih, p) async for ih, p in agen]
    else:
        yields = [(ih, p) async for ih, p in agen]
        assert yields == exp_yields

    if isinstance(exp_exception, AssertionError):
        assert DynamicInterval_mock.call_args_list == []
        assert partial_mock.call_args_list == []
        assert api._torrent_is_verifying.call_args_list == []
    else:
        assert DynamicInterval_mock.call_args_list == [call(
            name=infohash,
            min='interval min',
            max='interval max',
            progress_getter=partial_mock.return_value
        )]

        assert partial_mock.call_args_list == [call(api._get_verifying_progress, infohash)]

        call_count = list(reversed(torrent_is_verifying_results)).index(True)
        assert api._torrent_is_verifying.call_args_list == [call(infohash)] * call_count
