import pytest

from aiobtclientapi import constants
from aiobtclientapi.torrentdata import attrs


def test_name():
    class Foo(attrs.TorrentAttribute):
        name = 'foo'

    assert Foo.name == 'foo'
    assert attrs.TorrentAttribute.name is NotImplemented


def test_dependencies():
    class Foo(attrs.TorrentAttribute):
        dependencies = ('a', 'b', 'c')

    assert Foo.dependencies == ('a', 'b', 'c')
    assert attrs.TorrentAttribute.dependencies == ()


def test_get_value_with_missing_dependency():
    class Foo(attrs.TorrentAttribute):
        name = 'foo'
        dependencies = ('a', 'b', 'c')

        def _get_value(raw):
            assert raw == 'rawr'
            raise KeyError('asdf')

    with pytest.raises(AttributeError, match=r"^Missing dependency for attribute 'foo': 'asdf'$"):
        Foo.get_value('rawr')


def test_get_value_with_unimplemented_getter():
    class Foo(attrs.TorrentAttribute):
        name = 'foo'
        dependencies = ('a', 'b', 'c')

        def _get_value(raw):
            assert raw == 'rawr'
            return NotImplemented

    assert Foo.get_value('rawr') is constants.NOT_IMPLEMENTED


def test_get_value_normalizes_value():
    class Foo(attrs.TorrentAttribute):
        name = 'foo'
        dependencies = ('a', 'b', 'c')

        def _get_value(raw):
            assert raw == 'rawr'
            return 'hello'

        def _normalize_value(value):
            assert value == 'hello'
            return f'normalized:{value}'

    assert Foo.get_value('rawr') == 'normalized:hello'


def test__get_value():
    assert attrs.TorrentAttribute._get_value('rawr') is NotImplemented


def test__normalize_value():
    with pytest.raises(NotImplementedError, match=r"^TorrentAttribute\._normalize_value\(\) is not implemented$"):
        attrs.TorrentAttribute._normalize_value('rawr')
