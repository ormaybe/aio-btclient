import types
from unittest.mock import Mock, call

import pytest

from aiobtclientapi.torrentdata import attrs


def test__get_all_parents(mocker):
    class Foo(attrs.TorrentAttribute):
        name = 'foo'

    class Bar(attrs.TorrentAttribute):
        name = 'bar'

    find_subclasses_mock = mocker.patch('aiobtclientapi.utils.find_subclasses', return_value=(Foo, Bar))

    # This method is cached and may have been already called by other tests.
    attrs.TorrentAttributes._get_all_parents.cache_clear()

    for _ in range(3):
        subclasses = attrs.TorrentAttributes._get_all_parents()
        assert subclasses == {'foo': Foo, 'bar': Bar}

    assert find_subclasses_mock.call_args_list == [
        call(attrs.TorrentAttribute, attrs)
    ]


def test__get_not_implemented_attribute(mocker):
    class Foo(attrs.TorrentAttribute):
        name = 'foo'

    return_values = []
    for _ in range(3):
        return_values.append(attrs.TorrentAttributes._get_default_attribute(Foo))

    assert all(
        rv is return_values[0]
        for rv in return_values
    ), return_values

    assert return_values[0].__name__ == 'Foo_NotImplemented'
    assert return_values[0].__mro__ == (
        return_values[0],
        Foo,
        attrs.TorrentAttribute,
        object,
    )


@pytest.fixture
def create_mock_classes(mocker):
    def create_mock_classes():
        parents = {
            'foo': type('Foo', (attrs.TorrentAttribute,), {'name': 'foo', 'dependencies': ('f', 'o')}),
            'bar': type('Bar', (attrs.TorrentAttribute,), {'name': 'bar', 'dependencies': ('b', 'a', 'r')}),
            'baz': type('Baz', (attrs.TorrentAttribute,), {'name': 'baz', 'dependencies': ('b', 'a', 'z')}),
        }

        defaults = {
            parent: type(f'Default{parent.__name__}', (parent,), {})
            for parent in parents.values()
        }

        attributes = {
            'foo': type('Foo', (parents['foo'],), {'_get_value': lambda raw: 'Foo!'}),
            'bar': type('Bar', (parents['bar'],), {'_get_value': lambda raw: 'Bawr!'}),
            'baz': type('Baz', (parents['baz'],), {'_get_value': lambda raw: 'Bar!'}),
        }

        mocks = Mock()
        mocks.attach_mock(
            mocker.patch.object(attrs.TorrentAttributes, '_get_all_parents', return_value=parents),
            '_get_all_parents',
        )
        mocks.attach_mock(
            mocker.patch.object(attrs.TorrentAttributes, '_get_default_attribute', side_effect=(
                lambda parent: defaults[parent]
            )),
            '_get_default_attribute',
        )

        return types.SimpleNamespace(
            parents=parents,
            defaults=defaults,
            attributes=attributes,
            mocks=mocks,
        )

    return create_mock_classes


def test_from_module(create_mock_classes, mocker):
    rsrcs = create_mock_classes()
    rsrcs.mocks.attach_mock(
        mocker.patch('aiobtclientapi.utils.find_subclasses', return_value=(
            rsrcs.attributes['foo'],
            rsrcs.attributes['baz'],
        )),
        'find_subclasses',
    )
    module = types.ModuleType('my_module')

    assert attrs.TorrentAttributes.from_module(module) == (
        rsrcs.attributes['foo'],
        rsrcs.attributes['baz'],
        rsrcs.defaults[rsrcs.parents['bar']],
    )
    assert rsrcs.mocks.mock_calls == [
        call.find_subclasses(attrs.TorrentAttribute, module),
        call._get_all_parents(),
        call._get_default_attribute(rsrcs.parents['bar']),
    ]


def test___new___gets_invalid_attribute(create_mock_classes):
    rsrcs = create_mock_classes()
    with pytest.raises(ValueError, match=r"^Not a TorrentAttribute subclass: 'invalid'$"):
        attrs.TorrentAttributes(rsrcs.attributes['foo'], 'invalid', rsrcs.attributes['baz'])
    assert rsrcs.mocks.mock_calls == []


def test___new___gets_only_valid_attribute(create_mock_classes):
    rsrcs = create_mock_classes()
    return_value = attrs.TorrentAttributes(rsrcs.attributes['foo'], rsrcs.attributes['baz'])
    assert return_value == (rsrcs.attributes['foo'], rsrcs.attributes['baz'])
    assert rsrcs.mocks.mock_calls == []


def test_names(create_mock_classes):
    rsrcs = create_mock_classes()
    return_value = attrs.TorrentAttributes(rsrcs.attributes['foo'], rsrcs.attributes['baz'])
    assert return_value.names == ('foo', 'baz')
    assert rsrcs.mocks.mock_calls == []


def test_as_dict(create_mock_classes):
    rsrcs = create_mock_classes()
    return_value = attrs.TorrentAttributes(rsrcs.attributes['baz'], rsrcs.attributes['bar'])
    assert return_value.as_dict == {
        'baz': rsrcs.attributes['baz'],
        'bar': rsrcs.attributes['bar'],
    }
    assert rsrcs.mocks.mock_calls == []


def test_only(create_mock_classes):
    rsrcs = create_mock_classes()
    return_value = attrs.TorrentAttributes(*rsrcs.attributes.values())
    assert return_value == (
        rsrcs.attributes['foo'],
        rsrcs.attributes['bar'],
        rsrcs.attributes['baz'],
    )

    assert return_value.only('foo') == (rsrcs.attributes['foo'],)
    assert isinstance(return_value.only('foo'), attrs.TorrentAttributes)

    assert return_value.only('baz', 'bar') == (rsrcs.attributes['baz'], rsrcs.attributes['bar'])
    assert isinstance(return_value.only('baz', 'bar'), attrs.TorrentAttributes)

    assert return_value.only('baz', 'foo') == (rsrcs.attributes['baz'], rsrcs.attributes['foo'])
    assert isinstance(return_value.only('baz', 'foo'), attrs.TorrentAttributes)

    with pytest.raises(ValueError, match=r"No such attribute: 'asdf'"):
        return_value.only('baz', 'asdf', 'foo')

    assert rsrcs.mocks.mock_calls == []


def test_dependencies(create_mock_classes):
    rsrcs = create_mock_classes()
    assert attrs.TorrentAttributes(rsrcs.attributes['foo'], rsrcs.attributes['bar'], rsrcs.attributes['baz']).dependencies == (
        'a', 'b', 'f', 'o', 'r', 'z',
    )
    assert attrs.TorrentAttributes(rsrcs.attributes['bar'], rsrcs.attributes['baz']).dependencies == (
        'a', 'b', 'r', 'z',
    )
    assert attrs.TorrentAttributes(rsrcs.attributes['bar']).dependencies == (
        'a', 'b', 'r',
    )
    assert rsrcs.mocks.mock_calls == []
