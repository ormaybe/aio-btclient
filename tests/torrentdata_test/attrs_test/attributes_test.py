from aiobtclientapi.torrentdata import attrs, types


def run_boolean_tests(attribute, exp_name):
    testcases = (
        (1, True),
        (0, False),
        ('this', True),
        ('', False),
    )

    for value, exp_normalized in testcases:
        assert attribute.name == exp_name
        normalized = attribute._normalize_value(value)
        assert normalized is exp_normalized


def run_string_tests(attribute, exp_name):
    testcases = (
        ('hello', 'hello'),
        ('A\u030Asdf', '\u00C5sdf'),
    )
    for value, exp_normalized in testcases:
        assert attribute.name == exp_name
        normalized = attribute._normalize_value(value)
        assert normalized == exp_normalized
        assert isinstance(normalized, str)


def run_count_tests(attribute, exp_name):
    testcases = (
        (123, 123, int),
        (123.3, 123, int),
        (123.7, 123, int),
        (types.Count.UNKNOWN, types.Count.UNKNOWN, types.Count),
    )

    for value, exp_normalized, exp_type in testcases:
        assert attribute.name == exp_name
        normalized = attribute._normalize_value(value)
        assert normalized == exp_normalized
        assert isinstance(normalized, exp_type)


def test_Infohash():
    assert attrs.Infohash.name == 'infohash'
    normalized = attrs.Infohash._normalize_value('FFF0000000000000000000000000000000000fff')
    assert normalized == 'fff0000000000000000000000000000000000fff'
    assert isinstance(normalized, types.Infohash)


def test_IsConnected():
    run_boolean_tests(attrs.IsConnected, 'is_connected')


def test_IsDiscovering():
    run_boolean_tests(attrs.IsDiscovering, 'is_discovering')


def test_IsDownloading():
    run_boolean_tests(attrs.IsDownloading, 'is_downloading')


def test_IsIdle():
    run_boolean_tests(attrs.IsIdle, 'is_idle')


def test_IsIsolated():
    run_boolean_tests(attrs.IsIsolated, 'is_isolated')


def test_IsQueued():
    run_boolean_tests(attrs.IsQueued, 'is_queued')


def test_IsSeeding():
    run_boolean_tests(attrs.IsSeeding, 'is_seeding')


def test_IsStopped():
    run_boolean_tests(attrs.IsStopped, 'is_stopped')


def test_IsUploading():
    run_boolean_tests(attrs.IsUploading, 'is_uploading')


def test_IsVerifying():
    run_boolean_tests(attrs.IsVerifying, 'is_verifying')


def test_Name():
    run_string_tests(attrs.Name, 'name')


def test_PeersSeeding():
    run_count_tests(attrs.PeersSeeding, 'peers_seeding')
