import pytest

from aiobtclientapi.torrentdata import attrs


class Foo(attrs.TorrentAttribute):
    dependencies = ('f', 'o')

class Bar(attrs.TorrentAttribute):
    dependencies = ('b', 'a', 'r')

class Baz(attrs.TorrentAttribute):
    dependencies = ('b', 'a', 'z')


def test_valid_dependencies():
    joined = attrs.join_dependencies('a', 's', Bar, Baz, 'd', 'f')
    assert joined == ('a', 'b', 'd', 'f', 'r', 's', 'z')


def test_invalid_dependencies():
    with pytest.raises(ValueError, match=r'^Invalid dependency: 123$'):
        attrs.join_dependencies(Foo, 123, ['hello'])
