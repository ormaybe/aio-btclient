import itertools

import pytest

from aiobtclientapi.utils import constant


def get_constant_name(counter=itertools.count(1)):
    return f'CONSTANT_{next(counter)}'


@pytest.mark.parametrize(
    argnames='name, exp_name',
    argvalues=(
        ('FoO', 'Constant_FOO'),
        ('F-O-O-O', 'Constant_F_O_O_O'),
        ('F_O_O_O_O', 'Constant_F_O_O_O_O'),
        ('123FOO', 'Constant_123FOO'),
        ('F1O2O3', 'Constant_F1O2O3'),
    ),
)
def test_names(name, exp_name):
    value = 'some value'
    const = constant.get(name, value)
    assert const.name == name
    assert type(const).__name__ == exp_name
    assert type(const).__qualname__ == f'aiobtclientapi.utils.constant.get({name!r}, {value!r})'


@pytest.mark.parametrize(
    argnames='value, exp_parents',
    argvalues=(
        (123, (int, constant.Constant, object)),
        (12.3, (float, constant.Constant, object)),
        ('foo', (str, constant.Constant, object)),
        ((1, 'two', 3), (tuple, constant.Constant, object)),
    ),
)
def test_value_and_type(value, exp_parents):
    name = get_constant_name()
    const = constant.get(name, value)
    assert const == value
    assert type(const).__mro__[0] == type(const)
    assert type(const).__mro__[1:] == exp_parents


def test_caching():
    a = constant.get(get_constant_name(), 1)
    b = constant.get(get_constant_name(), 2)
    c = constant.get(get_constant_name(), 3)

    assert a is constant.get(a.name, 1)
    assert b is constant.get(b.name, 2)
    assert c is constant.get(c.name, 3)


@pytest.mark.parametrize(
    argnames='name_a, name_b',
    argvalues=(
        ('NAME_FOR_TESTING_DUPLICATES', 'NAME_FOR_TESTING_DUPLICATES'),
        ('NAME_FOR_TESTING_DUPLICATES', 'NAME/FOR/TESTING/DUPLICATES'),
    ),
)
def test_redefining_constant(name_a, name_b):
    const = constant.get(name_a, 1)
    with pytest.raises(RuntimeError, match=(
            '^Cannot create same constants with different values: '
            rf'<Constant {const.name}=1> != 2$'
    )):
        constant.get(name_b, 2)


def test_string_representations():
    name = get_constant_name()
    const = constant.get(name, 'this is foo')
    assert str(const) == 'this is foo'
    assert repr(const) == f"<Constant {name}='this is foo'>"
